/*
 * 
 * Water heating project for IoT course
 * Author: Tiago Dávila dos Santos
 * 
 */
#define MAX_MESSAGE_LENGTH 80
#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 600
#define INSTRUCTIONS_SIZE 2
#define SERVO_ELETRIC 45
#define SERVO_GAS 135


#include <Servo.h>
#include <TH02_dev.h>
#include "rgb_lcd.h"
#include <math.h>
#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"



char message[MAX_MESSAGE_LENGTH];
Servo mServo;

float mGasValue = 30;
float mEletricValue = 100;

int mWaterTemperature;
float mMoistureLevel = 200;

boolean mLocalButton = false;
bool mRemoteButton = false;


boolean DEBUG = false;
float mCurrentTemperature = 25;
float mTargetTemperature = 30;
int relayPort = 5;
int buttonPort = 0;
rgb_lcd lcd;
static char certDirectory[] = "certs";
// initialize the mqtt client
AWS_IoT_Client mqttClient;
IoT_Error_t rc = FAILURE;
int32_t i = 0;
float newGasValue;

int mRc = -999;

char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);
char *pJsonStringToUpdate;
float temperature = 0.0;

bool windowOpen = false;
jsonStruct_t windowActuator;
jsonStruct_t targetTemperatureHandler;
jsonStruct_t remoteButtonHandler;
jsonStruct_t gasCoastHandler;
jsonStruct_t eletricCoastHandler;


unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long lastDebounceTime2 = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
unsigned long debounceDelay2 = 2000;    // the debounce time; increase if the output flickers
int lastButtonState = LOW;   // the previous reading from the input pin

void setup() {
  prepareInterfacesComunication();
  setupAWS();
}

void loop() {
  while (readFromSerial()) {
    // Do nothing while reading from user input.
  }

  int reading = digitalRead(buttonPort);

  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  String heatingSystem;
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (digitalRead(buttonPort) == HIGH) {
      mLocalButton = !mLocalButton;

      if (DEBUG) {
        Serial.print("Local ");
        Serial.println(mLocalButton);
        Serial.print("Remote ");
        Serial.println(mRemoteButton);
        Serial.print("Current Temp ");
        Serial.println(mCurrentTemperature);
        Serial.print("Target Temp ");
        Serial.println(mTargetTemperature);  
      }
      
      if (mLocalButton || mRemoteButton && 
              (mCurrentTemperature < mTargetTemperature)) { 
                
        if (calculateGasValue() > mEletricValue) {
          setServoPosition(SERVO_ELETRIC);
          heatingSystem = "E";
        } else {
          setServoPosition(SERVO_GAS);
          heatingSystem = "G";
        }
      }

      setMoistureValue();
      updateCurrentTemperature();
      setRelayStatus(mLocalButton || mRemoteButton);
      showInformationOnLCD(mLocalButton || mRemoteButton, heatingSystem,
                          mCurrentTemperature, mTargetTemperature);
      mRc = 0;
    }
  }

  // This would only happen the first time.
  if (lastDebounceTime == 0) {
    setRelayStatus(false);
    lastDebounceTime = 1;
    showInformationOnLCD(mLocalButton || mRemoteButton, heatingSystem,
                          mCurrentTemperature, mTargetTemperature);
  }

  
  if ((millis() - lastDebounceTime2) > debounceDelay2) {
    lastDebounceTime2 = millis();  
    sendInformationToAWS();
  }
}

void prepareInterfacesComunication() {
  // This sets the BAUD rate to 9600
  Serial.begin(9600);
  // This will configure LCD with rows and columns.
  lcd.begin(16, 2);
  lcd.setRGB(255, 255, 125);
  // This will configure Servo
  mServo.attach(6);
  // This will configure Relay to be set as an OUTPUT
  pinMode(relayPort, OUTPUT);
  // This will configure Button to be set as an INPUT
  pinMode(buttonPort, INPUT);

  if (DEBUG) {
    Serial.println("Setup from Serial Comunication - DONE");
  }
}

void updateCurrentTemperature() {
  mCurrentTemperature = TH02.ReadTemperature();
}

bool readFromSerial() {
  while (Serial.available()) {
    char command = Serial.read();
    int index;
    char value[10] = "\0";
    for (index = 0; Serial.available(); index++) {
      value[index] = Serial.read();
      if (value[index] == '\n' || value[index] == ',') {
        value[index] = '\0';
        break;  // command termination character
      }
    }
    applyCommand(command, value);
    return true;
  }
  return false;
}

void applyCommand(char command, char value[]) {
  if (DEBUG) {
    Serial.print("Command -> ");
    Serial.print(command);
    Serial.print(" | Value -> ");
    Serial.println(atoi(value));  
  }
  
  switch (tolower(command)) {
    case 'g':
      // Update Value with Gas current value
      mGasValue = atof(value);
      break;
    case 'e':
      // Update mValue with Eletric current value
      mEletricValue = atof(value);
      break;
    case 't':
      // Update mTargetTemperature
      mTargetTemperature = atof(value);
      break;
    case 'r':
      // Used to toggle the value of the remote button
      mRemoteButton = atoi(value) == 1;
      break;
    case 'l':
      // Used to toggle the value of the local button
      mLocalButton = atoi(value) == 1;
      break;
    case 'w':
      // Current value of the water temperature
      mWaterTemperature = atof(value);
      break;
    case 'm':
      // Current value of the moisture level
      mMoistureLevel = atoi(value);
      break;
  }
}

void setServoPosition(int value) {
  // Use this method with SERVO_ELETRIC and SERVO_GAS
  mServo.write(value);
}

int setMoistureValue() {
  mMoistureLevel = analogRead(A3);
}

int setRelayStatus(boolean isOn) {
  if (isOn) {
    digitalWrite(relayPort, HIGH);
  } else {
    digitalWrite(relayPort, LOW);
  }
}

float calculateGasValue() {
  newGasValue = fabs(mGasValue * ((mMoistureLevel/1000.0)*8));
  
  if (DEBUG) {
    Serial.print("New value of mGasValue is - ");
    Serial.println(newGasValue);
  }

  return newGasValue;
}

void showInformationOnLCD(boolean isWorking, String heatingSystem,
                          float currentTemperature, float wantedTemperature) {
  lcd.clear();
  lcd.setCursor(0,0);
  String firstPosition = isWorking? heatingSystem : "O";
  lcd.print(firstPosition);

  lcd.setCursor(6,0);
  lcd.print(currentTemperature);

  lcd.setCursor(6,1);
  lcd.print(wantedTemperature);

  lcd.setCursor(12,0);
  lcd.print("C");

  lcd.setCursor(12,1);
  lcd.print("C");
}

void setupAWS() {
  targetTemperatureHandler.cb = targetTemperatureCallback;
  targetTemperatureHandler.pKey = "currentTemperature";
  targetTemperatureHandler.pData = &mCurrentTemperature;
  targetTemperatureHandler.type = SHADOW_JSON_FLOAT; 

  remoteButtonHandler.cb = remoteButtonCallback;
  remoteButtonHandler.pData = &mRemoteButton;
  remoteButtonHandler.pKey = "remoteButton";
  remoteButtonHandler.type = SHADOW_JSON_BOOL;

  gasCoastHandler.cb = gasCoastCallback;
  gasCoastHandler.pKey = "gasCoast";
  gasCoastHandler.pData = &newGasValue;
  gasCoastHandler.type = SHADOW_JSON_FLOAT;

  eletricCoastHandler.cb = eletricCoastCallback;
  eletricCoastHandler.pKey = "eletricCoast";
  eletricCoastHandler.pData = &mEletricValue;
  eletricCoastHandler.type = SHADOW_JSON_FLOAT;

  char rootCA[PATH_MAX + 1];
  char clientCRT[PATH_MAX + 1];
  char clientKey[PATH_MAX + 1];
  char CurrentWD[PATH_MAX + 1] = "sketch";

  snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
  snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
  snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

  ShadowInitParameters_t sp = ShadowInitParametersDefault;
  sp.pHost = AWS_IOT_MQTT_HOST;
  sp.port = AWS_IOT_MQTT_PORT;
  sp.pClientCRT = clientCRT;
  sp.pClientKey = clientKey;
  sp.pRootCA = rootCA;
  sp.enableAutoReconnect = false;
  sp.disconnectHandler = NULL;

  
  mRc = aws_iot_shadow_init(&mqttClient, &sp);
  if(SUCCESS != mRc) {
    IOT_ERROR("Shadow Connection Error");
    return;
  }

  ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
  scp.pMyThingName = AWS_IOT_MY_THING_NAME;
  scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
  scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);
  
  IOT_INFO("Shadow Connect");
  mRc = aws_iot_shadow_connect(&mqttClient, &scp);
  if(SUCCESS != mRc) {
    IOT_ERROR("Shadow Connection Error");
    return;
  }

  /*
   * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
   *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
   *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
   */
  rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
  if(SUCCESS != rc) {
    IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
    return;
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &remoteButtonHandler);
  rc = aws_iot_shadow_register_delta(&mqttClient, &targetTemperatureHandler);
  rc = aws_iot_shadow_register_delta(&mqttClient, &gasCoastHandler);
  rc = aws_iot_shadow_register_delta(&mqttClient, &eletricCoastHandler);

  if(SUCCESS != rc) {
    IOT_ERROR("Shadow Register Delta Error");
  }
}

void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                const char *pReceivedJsonDocument, void *pContextData) {
  IOT_UNUSED(pThingName);
  IOT_UNUSED(action);
  IOT_UNUSED(pReceivedJsonDocument);
  IOT_UNUSED(pContextData);

  if(SHADOW_ACK_TIMEOUT == status) {
    IOT_INFO("Update Timeout--");
  } else if(SHADOW_ACK_REJECTED == status) {
    IOT_INFO("Update RejectedXX");
  } else if(SHADOW_ACK_ACCEPTED == status) {
    IOT_INFO("Update Accepted !!");
  }
}

void remoteButtonCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.println("Delta Received - RemoteButton Updated");
}

void targetTemperatureCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.println("Delta Received - Target Temperature Updated");
}

void gasCoastCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.println("Delta Received - Gas Coast Updated");
}

void eletricCoastCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  Serial.println("Delta Received - Eletric Coast Updated");
}

void sendInformationToAWS() {
  // loop and publish a change in temperature
  if(NETWORK_ATTEMPTING_RECONNECT == mRc || NETWORK_RECONNECTED == mRc || SUCCESS == mRc) {
    rc = aws_iot_shadow_yield(&mqttClient, 200);
    if(NETWORK_ATTEMPTING_RECONNECT == mRc) {
      sleep(1);
      // If the client is attempting to reconnect we will skip the rest of the loop.
    }
    mRc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
    if(SUCCESS == mRc) {
      mRc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 4, &targetTemperatureHandler,
                                            &remoteButtonHandler, &gasCoastHandler, &eletricCoastHandler);
      if(SUCCESS == mRc) {
        mRc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
        if(SUCCESS == mRc) {
          IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
          mRc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                         ShadowUpdateStatusCallback, NULL, 4, true);
          Serial.print("Shadow Update ");
          Serial.println(mRc == SUCCESS? "Success" : "Failed");
        }
      }
    }
    sleep(1);
  } else {
    if(SUCCESS != mRc) {
      IOT_ERROR("An error occurred in the loop %d", rc);
    }
  
    IOT_INFO("Disconnecting");
    mRc = aws_iot_shadow_disconnect(&mqttClient);
  
    if(SUCCESS != mRc) {
      IOT_ERROR("Disconnect error %d", mRc);
    }
  }
}


